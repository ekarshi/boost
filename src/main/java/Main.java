import ai.learning.boost.AdaBoost;
import ai.learning.data.DataSet;
import ai.learning.data.DataType;

public class Main {

    public static void main(String[] args) {
        String fileName = "/Users/ekarshimitra/data/banknote.txt";
        DataSet dataSet = new DataSet.DataSetBuilder(fileName)
                .normalizeDataSet(false)
                .delimiter(",")
                .shuffleDataSet(false)
                .featureCount(4)
                .labelIndex(4)
                .trainToTestSplitPercent(0.3)
                .targetCategoricalClass("0")
                .targetClassValue(1)
                .FeatureDataType(0, DataType.REAL)
                .FeatureDataType(1, DataType.REAL)
                .FeatureDataType(2, DataType.REAL)
                .FeatureDataType(3, DataType.REAL)
                .build();

        AdaBoost adaBoost = new AdaBoost(0.97, 1000);
        adaBoost.trainModel(dataSet.getTrainSet(), dataSet.getTestSet(), dataSet.getMetaData());
    }
}

