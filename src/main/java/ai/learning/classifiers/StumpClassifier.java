package ai.learning.classifiers;

import ai.learning.constants.EvalSymbol;

import java.util.Arrays;

public class StumpClassifier implements IWeakClassifier {
    private double errorRate;
    private double votingPower;
    private int[] results;
    private final double threshold;
    private EvalSymbol symbol;
    private final int featureCol;
    private double accuracy;
    private int predict;

    public StumpClassifier(double threshold, EvalSymbol symbol, int featureCol) {
        this.threshold = threshold;
        this.symbol = symbol;
        this.featureCol = featureCol;
    }

    @Override
    public int applyClassifier(Object[] vector) {
        if (symbol == EvalSymbol.GT) {
            return (double) vector[featureCol] >= threshold ? predict : -predict;
        }
        return (double) vector[featureCol] <= threshold ? predict : -predict;
    }

    @Override
    public int[] applyClassifier(Object[][] vectors) {
        results = Arrays.stream(vectors).mapToInt(this::applyClassifier).toArray();
        return results;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(double errorRate) {
        this.errorRate = errorRate;
    }

    public double getVotingPower() {
        return votingPower;
    }

    public void setVotingPower(double votingPower) {
        this.votingPower = votingPower;
    }

    public int[] getResults() {
        return results;
    }

    public void setSymbol(EvalSymbol symbol) {
        this.symbol = symbol;
    }

    public void setPredict(int predict) {
        this.predict = predict;
    }

    @Override
    public String toString() {
        return "{Accuracy : " + accuracy + ", Error Rate " + errorRate + ", Alpha_T " + votingPower + " }";
    }
}
