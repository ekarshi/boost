package ai.learning.classifiers;

public interface IWeakClassifier {
    int applyClassifier(Object[] vector);

    int[] applyClassifier(Object[][] vectors);
}
