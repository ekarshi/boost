package ai.learning.classifiers;

import ai.learning.constants.Constants;
import ai.learning.constants.EvalSymbol;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class WeakLearner {
    private static final ToDoubleFunction<IWeakClassifier> ERROR_RATE = x -> ((StumpClassifier) x).getErrorRate();
    private static final int classA = 1;
    private static final int classB = -1;
    private final List<IWeakClassifier> classifiers;
    private final Object[][] data;
    private final JSONObject metaData;

    public WeakLearner(Object[][] data, JSONObject metaData) {
        classifiers = new CopyOnWriteArrayList<>();
        this.data = data;
        this.metaData = metaData;
        int featureCount = metaData.getInt(Constants.FEATURE_COUNT.getKeyName());

        IntStream.range(0, featureCount).parallel().forEach(feature -> {
            Arrays.stream(data).parallel().forEach(datum -> {
                double threshold = (double) datum[feature];
                stumpOnClass(threshold, feature, classA);
                stumpOnClass(threshold, feature, classB);
            });
        });
    }

    public IWeakClassifier getClassifier() {
        classifiers.stream().parallel().forEach(this::calStumpAccuracy);
        return classifiers.stream().min(Comparator.comparingDouble(ERROR_RATE)).get();
    }

    private void stumpOnClass(double threshold, int index, int predictClass) {
        StumpClassifier classifier = new StumpClassifier(threshold, EvalSymbol.GT, index);
        classifier.setPredict(predictClass);
        calStumpAccuracy(classifier);
        if (classifier.getAccuracy() < 0.5) {
            classifier.setSymbol(EvalSymbol.LT);
            classifier.setAccuracy(1. - classifier.getAccuracy());
        }
        classifiers.add(classifier);
    }

    private void calStumpAccuracy(IWeakClassifier classifier) {
        int[] results = classifier.applyClassifier(data);
        int weightIndex = metaData.getInt(Constants.WEIGHT_INDEX.getKeyName());
        int labelCol = metaData.getInt(Constants.LABEL_COL.getKeyName());
        long count = IntStream.range(0, results.length).filter(i -> results[i] == (int) data[i][labelCol]).count();
        double errorRate = IntStream.range(0, results.length).filter(i -> results[i] != (int) data[i][labelCol])
                .mapToDouble(i -> (double) data[i][weightIndex]).sum();
        double accuracy = (double) count / results.length;
        ((StumpClassifier) classifier).setAccuracy(accuracy);
        ((StumpClassifier) classifier).setErrorRate(errorRate);
    }
}
