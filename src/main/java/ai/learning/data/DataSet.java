package ai.learning.data;

import ai.learning.constants.Constants;
import org.json.JSONObject;

public class DataSet {
    private final JSONObject metaData;
    private final Object[][] trainSet;
    private final Object[][] testSet;

    public DataSet(JSONObject metaData, Object[][] trainSet, Object[][] testSet) {
        this.metaData = metaData;
        this.trainSet = trainSet;
        this.testSet = testSet;
    }

    public JSONObject getMetaData() {
        return metaData;
    }

    public Object[][] getTrainSet() {
        return trainSet;
    }

    public Object[][] getTestSet() {
        return testSet;
    }

    public static class DataSetBuilder {
        private final JSONObject metaData;

        public DataSetBuilder(String fileName) {
            metaData = new JSONObject();
            metaData.put(Constants.FILE_NAME.getKeyName(), fileName);
        }

        public DataSetBuilder featureCount(int featureCount) {
            metaData.put(Constants.FEATURE_COUNT.getKeyName(), featureCount);
            return this;
        }

        public DataSetBuilder labelIndex(int index) {
            metaData.put(Constants.LABEL_COL.getKeyName(), index);
            metaData.put(Constants.WEIGHT_INDEX.getKeyName(), index + 1);
            return this;
        }

        public DataSetBuilder trainToTestSplitPercent(double percent) {
            metaData.put(Constants.SPLIT_RATIO.getKeyName(), percent);
            return this;
        }

        public DataSetBuilder FeatureDataType(int index, DataType dataType) {
            metaData.put(String.valueOf(index), dataType);
            return this;
        }

        public DataSetBuilder normalizeDataSet(boolean shouldNormalize) {
            metaData.put(Constants.DATA_SET_NORMALIZED.getKeyName(), shouldNormalize);
            return this;
        }

        public DataSetBuilder delimiter(String delimiter) {
            metaData.put(Constants.DELIMITER.getKeyName(), delimiter);
            return this;
        }

        public DataSetBuilder shuffleDataSet(boolean shuffle) {
            metaData.put(Constants.SHUFFLE_DATA.getKeyName(), shuffle);
            return this;
        }

        public DataSetBuilder targetCategoricalClass(String className) {
            metaData.put(Constants.TARGET_CLASS_NAME.getKeyName(), className);
            return this;
        }

        public DataSetBuilder targetClassValue(int val) {
            metaData.put(Constants.TARGET_CLASS_VAL.getKeyName(), val);
            return this;
        }

        public DataSet build() {
            FileToMemory fileToMemory = new FileToMemory();
            DataHolder dataHolder = fileToMemory.processFile(metaData);
            return new DataSet(metaData, dataHolder.trainDataSet, dataHolder.testDataSet);
        }
    }
}
