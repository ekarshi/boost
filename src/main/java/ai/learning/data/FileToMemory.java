package ai.learning.data;

import ai.learning.constants.Constants;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class FileToMemory {

    public DataHolder processFile(JSONObject metaData) {
        String fileName = metaData.getString(Constants.FILE_NAME.getKeyName());
        try {
            DataHolder dataHolder = processContents(Files.lines(Paths.get(fileName)), metaData);
            mapTargetLabels(dataHolder.trainDataSet, metaData);
            mapTargetLabels(dataHolder.testDataSet, metaData);
            updateTypes(dataHolder.trainDataSet, metaData);
            updateTypes(dataHolder.testDataSet, metaData);
            return dataHolder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateTypes(Object[][] data, JSONObject metaData) {
        int featureCount = metaData.getInt(Constants.FEATURE_COUNT.getKeyName());
        for (int i = 0; i < featureCount; i++) {
            if (metaData.get(Integer.toString(i)) == DataType.REAL) {
                for (int j = 0; j < data.length; j++) {
                    data[j][i] = Double.parseDouble((String) data[j][i]);
                }
            }
        }
    }

    private void mapTargetLabels(Object[][] data, JSONObject metaData) {
        int labelIndex = metaData.getInt(Constants.LABEL_COL.getKeyName());
        String targetLabel = metaData.getString(Constants.TARGET_CLASS_NAME.getKeyName());
        int targetVal = metaData.getInt(Constants.TARGET_CLASS_VAL.getKeyName());
        for (Object[] datum : data) {
            datum[labelIndex] = ((String) datum[labelIndex]).equalsIgnoreCase(targetLabel) ? targetVal : -targetVal;
        }
    }

    private DataHolder processContents(Stream<String> lines, JSONObject metaData) {
        Object[] contents = lines.toArray();
        if (metaData.getBoolean(Constants.SHUFFLE_DATA.getKeyName()))
            Collections.shuffle(Arrays.asList(contents));
        int sampleCount = contents.length;
        int testSampleCount = (int) (sampleCount * metaData.getDouble(Constants.SPLIT_RATIO.getKeyName()));
        int trainSampleCount = sampleCount - testSampleCount;
        Object[][] testData = new Object[testSampleCount][metaData.getInt(Constants.FEATURE_COUNT.getKeyName()) + 1];
        Object[][] trainData = new Object[trainSampleCount][metaData.getInt(Constants.FEATURE_COUNT.getKeyName()) + 2];
        Set<Integer> testIndices = generateIndices(sampleCount, testSampleCount);
        int counterTestIndex = 0;
        int counterTrainIndex = 0;
        for (int i = 0; i < sampleCount; i++) {
            Object[] items = ((String) contents[i]).split(metaData.getString(Constants.DELIMITER.getKeyName()));
            if (testIndices.contains(i)) {
                System.arraycopy(items, 0, testData[counterTestIndex], 0, items.length);
                counterTestIndex += 1;
            } else {
                System.arraycopy(items, 0, trainData[counterTrainIndex], 0, items.length);
                trainData[counterTrainIndex][items.length] = 1. / trainSampleCount;
                counterTrainIndex += 1;
            }
        }
        return new DataHolder(testData, trainData);
    }

    private Set<Integer> generateIndices(int upperLimit, int count) {
        Set<Integer> indices = new HashSet<>();
        Random random = new Random();
        while (indices.size() != count) {
            indices.add(random.nextInt(upperLimit));
        }
        return indices;
    }
}
