package ai.learning.data;

public class DataHolder {
    public final Object[][] testDataSet;
    public final Object[][] trainDataSet;

    public DataHolder(Object[][] testDataSet, Object[][] trainDataSet) {
        this.testDataSet = testDataSet;
        this.trainDataSet = trainDataSet;
    }
}
