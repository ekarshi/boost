package ai.learning.constants;

public enum Constants {
    FEATURE_COUNT("feature count"),
    WEIGHT_INDEX("weight index"),
    LABEL_COL("label col"),
    SPLIT_RATIO("split percent"),
    DATA_SET_NORMALIZED("normalized"),
    DELIMITER("delimiter"),
    TARGET_CLASS_NAME("target class name"),
    TARGET_CLASS_VAL("target class value"),
    SHUFFLE_DATA("shuffled "),
    FILE_NAME("file");

    private final String keyName;

    Constants(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }
}
