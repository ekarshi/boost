package ai.learning.boost;

import ai.learning.classifiers.IWeakClassifier;
import ai.learning.classifiers.StumpClassifier;
import ai.learning.classifiers.WeakLearner;
import ai.learning.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class AdaBoost {
    private static final Logger LOGGER = LogManager.getLogger(AdaBoost.class);
    private final List<IWeakClassifier> classifiers;
    private final double targetAccuracy;
    private final int maxIterations;

    public AdaBoost(double targetAccuracy, int maxIterations) {
        this.targetAccuracy = targetAccuracy;
        this.maxIterations = maxIterations;
        this.classifiers = new LinkedList<>();
    }

    public void trainModel(Object[][] trainData, Object[][] testData, JSONObject metaData) {
        int weightIndex = metaData.getInt(Constants.WEIGHT_INDEX.getKeyName());
        int labelCol = metaData.getInt(Constants.LABEL_COL.getKeyName());
        int iterations = 0;
        WeakLearner weakLearner = new WeakLearner(trainData, metaData);

        while (iterations < maxIterations) {
            IWeakClassifier weakClassifier = weakLearner.getClassifier();
            double errorRate = ((StumpClassifier) weakClassifier).getErrorRate();
            if (errorRate > 0.5)
                return;

            double alphaT = 0.5 * Math.log((1. - errorRate) / errorRate);

            ((StumpClassifier) weakClassifier).setVotingPower(alphaT);
            updateDistribution(trainData, weakClassifier, weightIndex, labelCol);

            classifiers.add(weakClassifier);

            LOGGER.info("Best classifier selected : " + weakClassifier);
            if (testModel(testData, metaData) > targetAccuracy)
                break;

            iterations += 1;
        }
    }

    private void updateDistribution(Object[][] data, IWeakClassifier classifier, int weightIndex, int labelCol) {
        IntStream.range(0, data.length).forEach(i -> {
            double weight = (double) data[i][weightIndex];
            int actual = (int) data[i][labelCol];
            int predicted = ((StumpClassifier) classifier).getResults()[i];
            double alphaT = ((StumpClassifier) classifier).getVotingPower();
            data[i][weightIndex] = weight * Math.exp(-alphaT * predicted * actual);
        });

        double totalSum = Arrays.stream(data).mapToDouble(datum -> (double) datum[weightIndex]).sum();
        Arrays.stream(data).forEach(datum -> datum[weightIndex] = (double) datum[weightIndex] / totalSum);
    }

    private double testModel(Object[][] testData, JSONObject metaData) {
        int matched = 0;
        int labelCol = metaData.getInt(Constants.LABEL_COL.getKeyName());

        for (Object[] datum : testData) {
            matched = classifierPrediction(datum) == (int) datum[labelCol] ? matched + 1 : matched;
        }
        double accuracy = (double) matched / testData.length;
        LOGGER.info("Accuracy " + accuracy);
        return accuracy;
    }

    private int classifierPrediction(Object[] vec) {
        return classifiers.stream().
                mapToDouble(x -> x.applyClassifier(vec) * ((StumpClassifier) x).getVotingPower()).sum() > 0 ? 1 : -1;
    }
}
